require "./spec_helper"

describe Mig do
  it "has a version number" do
    Mig::VERSION.should_not be_empty
  end

  describe ".migrate" do
    context "without environment variable `DATABASE_URL`" do
      it "raises Exception" do
        ENV.delete("DATABASE_URL")

        expect_raises(Exception) do
          Mig.migrate.should eq Mig.migrate
        end
      end
    end

    context "with environment variable `DATABASE_URL`" do
      it "returns a singleton instance of Mig::Migrate class" do
        ENV["DATABASE_URL"] = "postgres://user:password@host:1234/dbname"

        Mig.migrate.should be_a Mig::Migrate
        Mig.migrate.should eq Mig.migrate
      end
    end
  end
end
