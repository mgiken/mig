require "../spec_helper"

describe Mig::DSN do
  describe ".parse" do
    context "with valid uri string" do
      it "returns a instance of Mig::DNS class" do
        Mig::DSN.parse("postgres://user:password@host:1234/dbname").should be_a Mig::DSN
      end
    end

    context "with invalid uri string" do
      it "raises URI::Error" do
        expect_raises(URI::Error) do
          Mig::DSN.parse("postgres://user:password@host:port/dbname")
        end
      end
    end

    context "with connection pool parameters" do
      it "returns a instance without connection pool parameters" do
        uri = "postgres://user:password@host:1234/dbname?" + %w[
          sslmode=disable
          initial_pool_size=1
          max_pool_size=1
          max_idle_pool_size=1
          checkout_timeout=1
          retry_attempts=1
          retry_delay=1
        ].join("&")

        Mig::DSN.parse(uri).to_s.should eq "postgres://user:password@host:1234/dbname?sslmode=disable"
      end
    end
  end

  describe "#dbname" do
    context "with dbname" do
      it "returns a dbname" do
        Mig::DSN.parse("postgres://user:password@host:1234/dbname").dbname.should eq "dbname"
      end
    end

    context "without dbname" do
      it "raises Exception" do
        expect_raises(Exception) do
          Mig::DSN.parse("postgres://user:password@host:1234").dbname
        end
      end
    end
  end

  describe "#user" do
    context "with user" do
      it "returns a user" do
        Mig::DSN.parse("postgres://user:password@host:1234/dbname").user.should eq "user"
      end
    end

    context "without user" do
      it "raises Exception" do
        expect_raises(Exception) do
          Mig::DSN.parse("postgres://:password@host:1234/dbname").user
        end
      end
    end
  end

  describe "#uri" do
    context "as user" do
      it "returns setted uri" do
        dsn = Mig::DSN.parse("postgres://user:password@host:1234/dbname")
        dsn.uri.to_s.should eq "postgres://user:password@host:1234/dbname?"
      end
    end

    context "as superuser" do
      it "returns uri for superuser" do
        dsn = Mig::DSN.parse("postgres://user:password@host:1234/dbname")
        dsn.uri(true).to_s.should eq "postgres://user:password@host:1234/postgres?"
      end
    end
  end
end
