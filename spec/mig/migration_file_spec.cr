require "../spec_helper"

describe Mig::MigrationFile do
  describe "#version" do
    context "when direction up" do
      it "returns a file name without extension" do
        Mig::MigrationFile.up("#{DATA_DIR}/migration.sql").version.should eq "migration"
      end
    end

    context "when direction down" do
      it "returns a file name without extension" do
        Mig::MigrationFile.down("#{DATA_DIR}/migration.sql").version.should eq "migration"
      end
    end
  end

  describe "#query" do
    context "when direction up" do
      it "returns a query for migrate" do
        Mig::MigrationFile.up("#{DATA_DIR}/migration.sql").query.should eq <<-EOS
          CREATE TABLE foo (
            foo_id INTEGER NOT NULL,
            PRIMARY KEY (foo_id)
          );
          EOS
      end
    end

    context "when direction down" do
      it "returns a query for rollback" do
        Mig::MigrationFile.down("#{DATA_DIR}/migration.sql").query.should eq <<-EOS
          DROP TABLE foo;
          EOS
      end
    end
  end

  describe "#with_tx" do
    context "when direction up" do
      context "with transaction" do
        it "returns true" do
          Mig::MigrationFile.up("#{DATA_DIR}/migration.sql").with_tx?.should be_true
        end
      end

      context "without transaction" do
        it "returns false" do
          Mig::MigrationFile.up("#{DATA_DIR}/migration-without-transaction.sql").with_tx?.should be_false
        end
      end
    end

    context "when direction down" do
      context "with transaction" do
        it "returns true" do
          Mig::MigrationFile.down("#{DATA_DIR}/migration.sql").with_tx?.should be_true
        end
      end

      context "without transaction" do
        it "returns false" do
          Mig::MigrationFile.down("#{DATA_DIR}/migration-without-transaction.sql").with_tx?.should be_false
        end
      end
    end
  end
end
