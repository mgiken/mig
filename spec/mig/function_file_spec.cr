require "../spec_helper"

describe Mig::FunctionFile do
  describe "#name" do
    it "returns a file name without extension" do
      Mig::FunctionFile.new("#{DATA_DIR}/function.sql").name.should eq "function"
    end
  end

  describe "#query" do
    it "returns a query" do
      Mig::FunctionFile.new("#{DATA_DIR}/function.sql").query.should eq <<-EOS
        CREATE OR REPLACE FUNCTION foo() RETURNS INTEGER AS $$
        BEGIN
          RETURN 1;
        END;
        $$ LANGUAGE plpgsql;
        EOS
    end
  end

  describe "#digest" do
    it "returns a digest of query" do
      Mig::FunctionFile.new("#{DATA_DIR}/function.sql").digest.should eq "5647f112b5ccad35b6fffd279691b8deba22252c"
    end
  end
end
