-- migrate up without transaction

CREATE TABLE foo (
  foo_id INTEGER NOT NULL,

  PRIMARY KEY (foo_id)
);

-- migrate down without transaction

DROP TABLE foo;
