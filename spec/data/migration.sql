-- migrate up

CREATE TABLE foo (
  foo_id INTEGER NOT NULL,

  PRIMARY KEY (foo_id)
);

-- migrate down

DROP TABLE foo;
