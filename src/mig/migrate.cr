require "csv"
require "digest"
require "yaml"

require "pg"
require "puerd"

require "./dsn"
require "./function_file"
require "./migration_file"

class Mig::Migrate
  getter dsn, base_dir, migrations_dir, functions_dir, views_dir, structure_sql, erd_puml, seeds_dir

  SKEL_SQL = {{ `cat #{__DIR__}/skel.sql`.stringify }}

  def initialize(db_url, @base_dir = "db")
    @dsn = DSN.parse(db_url)
    @migrations_dir = File.join(base_dir, "migrations")
    @functions_dir = File.join(base_dir, "functions")
    @views_dir = File.join(base_dir, "views")
    @structure_sql = File.join(base_dir, "structure.sql")
    @erd_puml = File.join(base_dir, "erd.puml")
    @seeds_dir = File.join(base_dir, "seeds")
  end

  def create
    with_db(true) do |db|
      db.exec(<<-SQL)
        CREATE DATABASE #{db.escape_identifier(dsn.dbname)}
                  OWNER #{db.escape_identifier(dsn.user)}
             LC_COLLATE 'C'
               LC_CTYPE 'C'
               TEMPLATE template0
        SQL
    end

    yield dsn.dbname
  end

  def drop
    with_db(true) do |db|
      db.exec("DROP DATABASE #{db.escape_identifier(dsn.dbname)}")
    end

    yield dsn.dbname
  end

  def new(name)
    ensure_dirs(base_dir, migrations_dir, functions_dir, views_dir, seeds_dir) do |dir|
      yield dir
    end

    t = Time.utc.to_s("%Y%m%d%H%M%S")
    f = File.join(migrations_dir, "#{t}-#{name}.sql")
    File.write(f, SKEL_SQL)

    yield f
  end

  def up(step : Int32 = 0)
    applied = false

    prepare

    with_db do |db|
      rs = db.query_all(<<-SQL, as: {name: String, digest: String?})
      SELECT "name",
             "digest"
        FROM "mig"."functions"
      SQL

      rs.each do |r|
        filename = File.join(functions_dir, "#{r[:name]}.sql")
        f = FunctionFile.new(filename)
        next if f.digest == r[:digest]

        with_tx(db) do |tx|
          tx.exec_all(f.query) unless f.empty?
          tx.exec(<<-SQL, f.digest, f.name)
            UPDATE "mig"."functions"
               SET "digest" = $1,
                   "applied_at" = NOW()
             WHERE "name" = $2
            SQL
        end

        yield filename
        applied = true
      end

      rs = db.query_all(<<-SQL, as: String)
       SELECT "version"
         FROM "mig"."migrations"
        WHERE "applied_at" IS NULL
        ORDER BY "version" ASC
        #{step == 0 ? "" : "LIMIT #{step}"}
       SQL

      rs.each do |r|
        filename = File.join(migrations_dir, "#{r}.sql")
        f = MigrationFile.up(filename)

        with_tx(db, f.with_tx?) do |tx|
          tx.exec_all(f.query) unless f.empty?
          tx.exec(<<-SQL, f.version)
            UPDATE "mig"."migrations"
               SET "applied_at" = NOW()
             WHERE "version" = $1
            SQL
        end

        yield filename
        applied = true
      end

      rs = db.query_all(<<-SQL, as: {name: String, digest: String?})
      SELECT "name",
             "digest"
        FROM "mig"."views"
      SQL

      rs.each do |r|
        filename = File.join(views_dir, "#{r[:name]}.sql")
        f = FunctionFile.new(filename)
        next if f.digest == r[:digest]

        with_tx(db) do |tx|
          tx.exec_all(f.query) unless f.empty?
          tx.exec(<<-SQL, f.digest, f.name)
            UPDATE "mig"."views"
               SET "digest" = $1,
                   "applied_at" = NOW()
             WHERE "name" = $2
            SQL
        end

        yield filename
        applied = true
      end
    end

    applied
  end

  def down(step = 1)
    applied = false

    prepare

    with_db do |db|
      rs = db.query_all(<<-SQL, as: {String})
        SELECT "version"
          FROM "mig"."migrations"
         WHERE "applied_at" IS NOT NULL
         ORDER BY "version" DESC
         #{step == 0 ? "" : "LIMIT #{step}"}
        SQL

      rs.each do |r|
        filename = File.join(migrations_dir, "#{r}.sql")
        f = MigrationFile.down(filename)

        with_tx(db, f.with_tx?) do |tx|
          tx.exec_all(f.query) unless f.empty?
          tx.exec(<<-SQL, f.version)
            UPDATE "mig"."migrations"
               SET "applied_at" = NULL
             WHERE "version" = $1
            SQL
        end

        yield filename
        applied = true
      end
    end

    applied
  end

  def dump
    old_digest = File.exists?(structure_sql) ? Digest::SHA1.hexdigest(File.read(structure_sql)) : ""

    cmd = "pg_dump"
    args = [dsn.to_s, "-s", "-x", "-O", "-f", structure_sql]
    ero = IO::Memory.new

    st = Process.run(cmd, args, error: ero)
    raise ero.to_s.chomp unless st.success?

    new_digest = Digest::SHA1.hexdigest(File.read(structure_sql))

    return if old_digest == new_digest
    yield structure_sql, old_digest.empty?
  end

  def delete_dump
    return unless File.exists?(structure_sql)

    File.delete(structure_sql)
    yield structure_sql
  end

  def erd
    old_digest = File.exists?(erd_puml) ? Digest::SHA1.hexdigest(File.read(erd_puml)) : ""

    File.write(erd_puml, Puerd::ERD.load(structure_sql))

    new_digest = Digest::SHA1.hexdigest(File.read(erd_puml))

    return if old_digest == new_digest
    yield erd_puml, old_digest.empty?
  end

  def delete_erd
    return unless File.exists?(erd_puml)

    File.delete(erd_puml)
    yield erd_puml
  end

  def seed
    with_db do |db|
      with_tx(db) do |tx|
        # temporarily disable all triggers
        tx.exec("SET SESSION_REPLICATION_ROLE = REPLICA")

        Dir.glob("#{seeds_dir}/*.csv").reverse.each do |filename|
          keys = [] of Array(String)
          vals = [] of Array(String?)
          File.open(filename) do |file|
            csv = CSV.new(file, true, true)
            keys = csv.headers

            csv.each do |it|
              vals << it.row.to_a.map { |x| x == "NULL" ? nil : x }
            end
          end

          table = db.escape_identifier(File.basename(filename, ".csv"))

          n = 0
          keys.each_slice(500) do |ks|
            vals.each_slice(500) do |vs|
              r = tx.exec(<<-SQL, args: vs.flatten)
                INSERT INTO #{table} (#{ks.join(",")})
                VALUES #{placeholders(ks.size, vs.size)}
                ON CONFLICT DO NOTHING
                SQL
              n += r.rows_affected
            end
          end

          yield filename, n if n > 0
        end

        # enable all triggers
        tx.exec("SET SESSION_REPLICATION_ROLE = DEFAULT")
      end
    end
  end

  private def ensure_dirs(*dirs)
    dirs.each do |dir|
      next if Dir.exists?(dir)

      Dir.mkdir(dir)
      yield dir
    end
  end

  private def prepare
    with_db do |db|
      db.exec(<<-SQL)
        CREATE SCHEMA IF NOT EXISTS "mig";
        SQL

      db.exec(<<-SQL)
        CREATE TABLE IF NOT EXISTS "mig"."migrations" (
          "version"    TEXT        NOT NULL,
          "applied_at" TIMESTAMPTZ     NULL,

          PRIMARY KEY ("version")
        )
        SQL

      db.exec(<<-SQL)
        CREATE TABLE IF NOT EXISTS "mig"."functions" (
          "name"       TEXT        NOT NULL,
          "digest"     TEXT            NULL,
          "applied_at" TIMESTAMPTZ     NULL,

          PRIMARY KEY ("name")
        )
        SQL

      db.exec(<<-SQL)
        CREATE TABLE IF NOT EXISTS "mig"."views" (
          "name"       TEXT        NOT NULL,
          "digest"     TEXT            NULL,
          "applied_at" TIMESTAMPTZ     NULL,

          PRIMARY KEY ("name")
        )
        SQL

      if (it = collect_files(migrations_dir)) && it.size > 0
        db.exec(<<-SQL, args: it)
          INSERT INTO "mig"."migrations"
                      ("version")
               VALUES #{placeholders(1, it.size)}
                   ON CONFLICT DO NOTHING
          SQL
      end

      if (it = collect_files(functions_dir)) && it.size > 0
        db.exec(<<-SQL, args: it)
          INSERT INTO "mig"."functions"
                      (name)
               VALUES #{placeholders(1, it.size)}
                   ON CONFLICT DO NOTHING
          SQL
      end

      if (it = collect_files(views_dir)) && it.size > 0
        db.exec(<<-SQL, args: it)
          INSERT INTO "mig"."views"
                      (name)
               VALUES #{placeholders(1, it.size)}
                   ON CONFLICT DO NOTHING
          SQL
      end
    end
  end

  private def collect_files(dir)
    Dir.glob("#{dir}/*.sql").map { |x| File.basename(x, ".sql") }
  end

  private def placeholders(col_n, row_n)
    n = 1
    (0...row_n).map do |i|
      i = i * col_n + 1
      n = i + col_n
      "(" + (i...n).map { |j| "$#{j}" }.join(",") + ")"
    end.join(",")
  end

  private def with_db(as_superuser? = false)
    DB.connect(dsn.uri(as_superuser?)) do |conn|
      yield conn
    end
  end

  private def with_tx(db, with_tx? = true)
    return yield db unless with_tx?

    db.transaction do |tx|
      yield tx.connection
    end
  end
end
