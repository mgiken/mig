require "uri"

class Mig::DSN
  forward_missing_to @uri
  delegate to_s, to: @uri

  SUPPORTED_PARAMS = %w[
    host
    port
    dbname
    user
    password
    passfile
    channel_binding
    connect_timeout
    client_encoding
    options
    application_name
    fallback_application_name
    keepalives
    keepalives_idle
    keepalives_interval
    keepalives_count
    tcp_user_timeout
    tty
    replication
    gssencmode
    sslmode
    requiressl
    sslcompression
    sslcert
    sslkey
    sslpassword
    sslrootcert
    sslcrl
    requirepeer
    ssl_max_protocol_version
    krbsrvname
    gsslib
    service
    target_session_attrs
  ]

  def initialize(@uri : URI)
    # remove unsupported params
    params = HTTP::Params.parse(@uri.query || "")
    params.each do |k, _|
      params.delete_all(k) unless SUPPORTED_PARAMS.includes?(k)
    end
    @uri.query = params.to_s
  end

  def dbname
    @uri.path.to_s.lstrip('/').tap do |it|
      raise "`DBNAME` is not defined: #{to_s}" if it.empty?
    end
  end

  def user
    @uri.user.to_s.tap do |it|
      raise "`USER` is not defined: #{to_s}" if it.empty?
    end
  end

  def uri(as_superuser? = false)
    @uri.dup.tap do |it|
      it.path = "/postgres" if as_superuser?
    end
  end

  def self.parse(database_url : String)
    new(URI.parse(database_url))
  end
end
