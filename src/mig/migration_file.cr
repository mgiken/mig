class Mig::MigrationFile
  getter version : String
  getter query : String
  getter? with_tx : Bool

  delegate empty?, to: @query

  def self.up(filename)
    new(filename, :up)
  end

  def self.down(filename)
    new(filename, :down)
  end

  def initialize(filename, direction)
    @version = File.basename(filename, ".sql")
    @query, @with_tx = parse(filename, direction)
  end

  private def parse(filename, direction)
    lines = {:up => [] of String, :down => [] of String}
    with_tx = {:up => true, :down => true}

    key = nil
    File.each_line(filename) do |line|
      next if line.blank?

      if line.starts_with?("-- migrate up")
        key = :up
        with_tx[key] = !line.includes?("without transaction")
        next
      end

      if line.starts_with?("-- migrate down")
        key = :down
        with_tx[key] = !line.includes?("without transaction")
        next
      end

      lines[key] << line
    end

    {lines[direction].join("\n"), with_tx[direction]}
  end
end
