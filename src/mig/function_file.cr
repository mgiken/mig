require "digest"

class Mig::FunctionFile
  getter name : String
  getter query : String

  delegate empty?, to: @query

  def initialize(filename)
    @name = File.basename(filename, ".sql")
    @query = parse(filename)
  end

  def digest
    Digest::SHA1.hexdigest(query)
  end

  private def parse(filename)
    lines = [] of String

    File.each_line(filename) do |line|
      lines << line unless line.blank?
    end

    lines.join("\n")
  end
end
