require "cliche"

require "./mig"

sub "create" do
  description "create database"

  Mig.migrate.create { |x| success("create database", x.inspect) }
end

sub "drop" do
  description "drop database"

  exit(1) unless confirm("Are you sure you want to drop database?")

  Mig.migrate.drop { |x| danger("drop database", x.inspect) }
  Mig.migrate.delete_erd { |x| danger("delete", x) }
  Mig.migrate.delete_dump { |x| danger("delete", x) }
end

sub "new" do
  description "generate a new migration file"

  arguments *name

  Mig.migrate.new(name.join('-')) { |x| success("create", x) }
end

sub "up" do
  description "migrate to the latest version"

  option step = 0, "s 0|step=0|the number of migrations (0 = unlimited)"
  option dump = true, "no-dump|skip dump schema file"

  Mig.migrate.up(step) { |x| success("apply", x) } || return

  return unless dump

  Mig.migrate.dump { |x, y| y ? success("create", x) : warning("update", x) }
  Mig.migrate.erd { |x, y| y ? success("create", x) : warning("update", x) }
end

sub "down" do
  description "rollback the most recent migration"

  option step = 1, "s 1|step=1|the number of migrations (0 = unlimited)"
  option dump = true, "no-dump|skip dump schema file"

  Mig.migrate.down(step) { |x| danger("rollback", x) } || return

  return unless dump

  Mig.migrate.dump { |x, y| y ? success("create", x) : warning("update", x) }
  Mig.migrate.erd { |x, y| y ? success("create", x) : warning("update", x) }
end

sub "dump" do
  description "write the database schema to disk"

  Mig.migrate.dump { |x, y| y ? success("create", x) : warning("update", x) }
end

sub "erd" do
  description "write the PlantUML ERD to disk"

  Mig.migrate.dump { |x, y| y ? success("create", x) : warning("update", x) }
  Mig.migrate.erd { |x, y| y ? success("create", x) : warning("update", x) }
end

sub "redo" do
  description "reapply the last migration"

  option step = 1, "s 1|step=1|the number of migrations (0 = unlimited)"
  option dump = true, "no-dump|skip dump schema file"

  Mig.migrate.down(step) { |x| danger("rollback", x) } || return
  Mig.migrate.up(step) { |x| success("apply", x) } || return

  return unless dump

  Mig.migrate.dump { |x, y| y ? success("create", x) : warning("update", x) }
  Mig.migrate.erd { |x, y| y ? success("create", x) : warning("update", x) }
end

sub "reset" do
  description "resets database"

  option dump = true, "no-dump|skip dump schema file"

  exit(1) unless confirm("Are you sure you want to reset database?")

  Mig.migrate.drop { |x| danger("drop database", x.inspect) }
  Mig.migrate.delete_erd { |x| danger("delete", x) }
  Mig.migrate.delete_dump { |x| danger("delete", x) }
  Mig.migrate.create { |x| success("create database", x.inspect) }
  Mig.migrate.up { |x| success("apply", x) } || return

  return unless dump

  Mig.migrate.dump { |x, y| y ? success("create", x) : warning("update", x) }
  Mig.migrate.erd { |x, y| y ? success("create", x) : warning("update", x) }
end

sub "seed" do
  description "write the PlantUML ERD to disk"

  Mig.migrate.seed { |x, y| success("import", "#{x} (#{y})") }
end

main do
  description "database migration tool"

  print_usage_and_exit
end
