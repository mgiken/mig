require "./mig/migrate"

module Mig
  VERSION = {{ `shards version #{__DIR__}`.stringify.chomp }}

  def self.migrate
    @@migrate ||= if it = ENV["DATABASE_URL"]
                    Mig::Migrate.new(it)
                  else
                    raise "environment variable `DATABASE_URL` not defined"
                  end
  end
end
